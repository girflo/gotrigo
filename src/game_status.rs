use crate::matrix::Matrix;
use bevy::prelude::Resource;
use bevy::time::Stopwatch;
use bevy::utils::Duration;

pub const MAX_DROP_SPEED: f32 = 0.05;

#[derive(Resource)]
pub struct GameStatus {
    drop_speed: f32,
    variable_drop_speed: f32,
    last_drop: Stopwatch,
    placed_matrix: Matrix,
    rotations: (i8, i8),
    pub game_over: bool,
}

impl Default for GameStatus {
    fn default() -> Self {
        Self {
            drop_speed: 1.,
            variable_drop_speed: 1.,
            last_drop: Stopwatch::new(),
            placed_matrix: Matrix::new(),
            rotations: (1, 1),
            game_over: false,
        }
    }
}

impl GameStatus {
    pub fn increase_rotations(&mut self) {
        if self.rotations == (1, 1) {
            self.rotations = (1, -1)
        } else if self.rotations == (1, -1) {
            self.rotations = (-1, -1)
        } else if self.rotations == (-1, -1) {
            self.rotations = (-1, 1)
        } else if self.rotations == (-1, 1) {
            self.rotations = (1, 1)
        }
    }

    pub fn reset_rotations(&mut self) {
        self.rotations = (1, 1);
    }

    pub fn collide(&mut self, x_position: f32, y_position: f32) -> bool {
        self.placed_matrix.collide(x_position, y_position)
    }

    pub fn activate_drop_speed(&mut self) {
        self.variable_drop_speed = MAX_DROP_SPEED;
    }

    pub fn deactivate_drop_speed(&mut self) {
        self.variable_drop_speed = self.drop_speed;
    }

    pub fn drop_needed(&self) -> bool {
        self.last_drop.elapsed_secs() > self.variable_drop_speed
    }

    pub fn reset_drop(&mut self) {
        self.last_drop.reset();
    }

    pub fn tick_drop(&mut self, time_delta: Duration) {
        self.last_drop.tick(time_delta);
    }

    pub fn rotation_x(&self) -> f32 {
        self.rotations.0 as f32
    }

    pub fn rotation_y(&self) -> f32 {
        self.rotations.1 as f32
    }

    pub fn place_on_board(&mut self, x_position: f32, y_position: f32, color_number: u8) {
        self.placed_matrix
            .place(x_position, y_position, color_number);
    }
}
