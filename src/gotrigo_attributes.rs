use rand::Rng;

pub const TRIGO_ASSETS: [&str; 2] = ["shub.png", "shuw.png"];

pub struct GotrigoAttributes<'a> {
    pub x: f32,
    pub y: f32,
    pub z: f32,
    pub color: u8,
    pub asset: &'a str,
}

impl GotrigoAttributes<'_> {
    pub fn new(position: u8) -> Self {
        let rdm_number = Self::generate_color();
        Self {
            x: Self::x_from_position(position),
            y: 320.,
            z: 1.,
            color: rdm_number + 1,
            asset: TRIGO_ASSETS[rdm_number as usize],
        }
    }

    fn x_from_position(position: u8) -> f32 {
        match position {
            0 => 32.,
            1 => 0.,
            _ => -32.,
        }
    }

    fn generate_color() -> u8 {
        let mut rng = rand::thread_rng();
        rng.gen_range(0..2)
    }
}
