use bevy::prelude::*;
mod game_status;
use game_status::GameStatus;
mod gotrigo_attributes;
use gotrigo_attributes::GotrigoAttributes;
mod matrix;

#[derive(Component)]
struct Dropable;

#[derive(Component)]
struct Next;

#[derive(Component)]
struct Color {
    color_number: u8,
}

fn main() {
    App::new()
        .init_resource::<GameStatus>()
        .add_plugins(DefaultPlugins)
        .add_startup_system(setup)
        .add_system(spawn_trigo)
        .add_system(sprite_movement)
        .add_system(keyboard_input_system)
        .run();
}

fn setup(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands.spawn(Camera2dBundle::default());
    commands.spawn(SpriteBundle {
        texture: asset_server.load("bg.png"),
        transform: Transform::from_xyz(90., 16., 0.),
        ..default()
    });
}

fn keyboard_input_system(
    keyboard_input: Res<Input<KeyCode>>,
    mut falling_trigo: Query<(&mut Transform, &mut Dropable)>,
    mut game_status: ResMut<GameStatus>,
) {
    let mut collision = false;
    if keyboard_input.just_pressed(KeyCode::Space) {
        let mut i = 0;
        let rotation_y = game_status.rotation_y() * 32.;
        let rotation_x = game_status.rotation_x() * 32.;

        for (transform, _) in &mut falling_trigo {
            collision = collision
                || (i == 0
                    && game_status.collide(
                        transform.translation.x - rotation_x,
                        transform.translation.y + rotation_y,
                    ))
                || (i == 2
                    && game_status.collide(
                        transform.translation.x + rotation_x,
                        transform.translation.y - rotation_y,
                    ));

            i += 1;
        }
        if !collision {
            let mut j = 0;

            for (mut transform, _) in &mut falling_trigo {
                if j == 0 {
                    transform.translation.x -= rotation_x;
                    transform.translation.y += rotation_y;
                } else if j == 2 {
                    transform.translation.x += rotation_x;
                    transform.translation.y -= rotation_y;
                }
                j += 1;
            }
            game_status.increase_rotations();
        }
    }
    if keyboard_input.just_pressed(KeyCode::Down) {
        game_status.activate_drop_speed();
    }
    if keyboard_input.just_released(KeyCode::Down) {
        game_status.deactivate_drop_speed();
    }
    if keyboard_input.just_pressed(KeyCode::Left) {
        for (transform, _) in &mut falling_trigo {
            collision = collision
                || game_status.collide(transform.translation.x - 32., transform.translation.y)
        }
        if !collision {
            for (mut transform, _) in &mut falling_trigo {
                transform.translation.x -= 32.;
            }
        }
    }
    if keyboard_input.just_pressed(KeyCode::Right) {
        for (transform, _) in &mut falling_trigo {
            collision = collision
                || game_status.collide(transform.translation.x + 32., transform.translation.y)
        }
        if !collision {
            for (mut transform, _) in &mut falling_trigo {
                transform.translation.x += 32.;
            }
        }
    }
}

fn spawn_trigo(
    game_status: ResMut<GameStatus>,
    mut commands: Commands,
    falling_trigo: Query<&Dropable>,
    mut next_trigo: Query<(Entity, &mut Transform, &Next)>,
    asset_server: Res<AssetServer>,
) {
    if game_status.game_over == false {
        if falling_trigo.is_empty() {
            for (entity, mut transform, _) in &mut next_trigo {
                transform.translation.y += 90.0;
                transform.translation.x -= 300.0;
                commands.entity(entity).insert(Dropable);
                commands.entity(entity).remove::<Next>();
            }
        }
        if next_trigo.is_empty() {
            for i in 0..3 {
                let gtg = GotrigoAttributes::new(i);
                commands.spawn((
                    SpriteBundle {
                        texture: asset_server.load(gtg.asset),
                        transform: Transform::from_xyz(gtg.x + 300., gtg.y - 90., gtg.z),
                        ..default()
                    },
                    Color {
                        color_number: gtg.color,
                    },
                    Next {},
                ));
            }
        }
    } else {
        commands.spawn(SpriteBundle {
            texture: asset_server.load("game_over.png"),
            transform: Transform::from_xyz(0., 16., 2.),
            ..default()
        });
    }
}

fn sprite_movement(
    mut commands: Commands,
    time: Res<Time>,
    mut dropping_unigos: Query<(Entity, &mut Transform, &Dropable, &Color)>,
    mut game_status: ResMut<GameStatus>,
) {
    if game_status.drop_needed() {
        let mut collision = false;

        for (_, transform, _, _) in &dropping_unigos {
            let next_y_position = transform.translation.y - 32.;
            collision = collision || game_status.collide(transform.translation.x, next_y_position)
        }
        if collision {
            for (entity, transform, _, color) in &mut dropping_unigos {
                game_status.place_on_board(
                    transform.translation.x,
                    transform.translation.y,
                    color.color_number,
                );
                if transform.translation.y == 320. {
                    game_status.game_over = true;
                }
                commands.entity(entity).remove::<Dropable>();
            }
            game_status.reset_rotations();
        } else {
            for (_, mut transform, _, _) in &mut dropping_unigos {
                transform.translation.y -= 32.0;
            }
        }
        game_status.reset_drop();
    }
    game_status.tick_drop(time.delta());
}
