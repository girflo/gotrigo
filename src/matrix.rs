pub struct Matrix {
    rows: [[u8; 20]; 11],
}

impl Matrix {
    pub fn new() -> Self {
        Self {
            rows: [[0; 20]; 11],
        }
    }

    pub fn get(&self, x_position: f32, y_position: f32) -> Option<u8> {
        let val = self
            .rows
            .get(self.col_from_x(x_position))?
            .get(self.row_from_y(y_position))?;
        Some(*val)
    }

    pub fn place(&mut self, x_position: f32, y_position: f32, color_number: u8) {
        let x = self.col_from_x(x_position);
        let y = self.row_from_y(y_position);
        self.rows[x][y] = color_number;
    }

    pub fn collide(&mut self, x_position: f32, y_position: f32) -> bool {
        let position_content = self.get(x_position, y_position).unwrap_or(0);
        x_position <= -192.
            || x_position >= 192.
            || y_position <= -304.
            || y_position >= 336.
            || position_content >= 1
    }

    fn col_from_x(&self, x_position: f32) -> usize {
        let col = (x_position + 160.) / 32.;
        col as usize
    }

    fn row_from_y(&self, y_position: f32) -> usize {
        let row = (y_position + 304.) / 32.;
        row as usize
    }
}
